package com.example.krasnononov_l2_t2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Layout;
import android.view.Menu;
import android.view.MenuInflater;

import com.google.android.material.bottomappbar.BottomAppBar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayList<Item> persons = new ArrayList<>();
        persons.add(new Item(R.drawable.photo, "Артем", "24"));
        persons.add(new Item(R.drawable.photo, "Артем", "24"));
        persons.add(new Item(R.drawable.photo, "Артем", "24"));
        persons.add(new Item(R.drawable.photo, "Артем", "24"));
        persons.add(new Item(R.drawable.photo, "Артем", "24"));
        persons.add(new Item(R.drawable.photo, "Артем", "24"));
        persons.add(new Item(R.drawable.photo, "Артем", "24"));
        persons.add(new Item(R.drawable.photo, "Артем", "24"));
        persons.add(new Item(R.drawable.photo, "Артем", "24"));
        persons.add(new Item(R.drawable.photo, "Артем", "24"));
        persons.add(new Item(R.drawable.photo, "Артем", "24"));
        persons.add(new Item(R.drawable.photo, "Артем", "24"));
        persons.add(new Item(R.drawable.photo, "Артем", "24"));
        persons.add(new Item(R.drawable.photo, "Артем", "24"));

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        adapter = new Adapter(persons);
        manager = new LinearLayoutManager(this){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_to_lesson2, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
