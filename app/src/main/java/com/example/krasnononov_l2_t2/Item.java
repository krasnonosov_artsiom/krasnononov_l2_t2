package com.example.krasnononov_l2_t2;

public class Item {

    private int imageView;
    private String name;
    private String age;

    public Item(int imageView, String name, String age) {
        this.imageView = imageView;
        this.name = name;
        this.age = age;
    }

    public int getImageView() {
        return imageView;
    }

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }
}
